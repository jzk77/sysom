#!/bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-diagnosis

init_conf() {
    cp ${SERVICE_NAME}.ini /etc/supervisord.d/
    ###change the install dir base on param $1###
    mkdir -p ${WEB_HOME}/diagnose/rca
    sed -i "s;/usr/local/sysom;${APP_HOME};g" /etc/supervisord.d/${SERVICE_NAME}.ini
    cpu_num=`cat /proc/cpuinfo | grep processor | wc -l`
    sed -i "s/threads = 3/threads = $cpu_num/g" ${SERVICE_HOME}/conf/gunicorn.py
}

init_app() {
    init_conf
    bash -x $BaseDir/db_migrate.sh
    ###if supervisor service started, we need use "supervisorctl update" to start new conf####
    supervisorctl update
}

init_app

# Start
bash -x $BaseDir/start.sh
