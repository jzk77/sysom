#!/bin/bash
SERVICE_SCRIPT_DIR=$(basename $(dirname $0))
SERVICE_HOME=${MICROSERVICE_HOME}/${SERVICE_SCRIPT_DIR}
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-hotfix-builder
NFS_SERVER_IP=${SERVER_LOCAL_IP}

mount_nfs()
{
    HOTFIX_NFS_HOME=${SERVER_HOME}/hotfix_builder/hotfix-nfs
    LOCAL_NFS_HOME=${SERVER_HOME}/builder/hotfix

    mkdir -p ${LOCAL_NFS_HOME}

    result=$(systemctl is-active nfs-server)
    if [[ $result =~ "inactive" ]]; then
        systemctl start nfs-server
    fi

    sudo umount $LOCAL_NFS_HOME # Remove the mounted directory in case it mounted before
    sudo mount -t nfs ${NFS_SERVER_IP}:${HOTFIX_NFS_HOME} ${LOCAL_NFS_HOME} || exit 1
}

init_conf() {
    cp ${SERVICE_NAME}.ini /etc/supervisord.d/
    ###change the install dir base on param $1###
    sed -i "s;/usr/local/sysom;${APP_HOME};g" /etc/supervisord.d/${SERVICE_NAME}.ini
    cpu_num=$(cat /proc/cpuinfo | grep processor | wc -l)
    sed -i "s/threads = 3/threads = $cpu_num/g" ${SERVICE_HOME}/conf/gunicorn.py
}

init_app() {
    init_conf
    mount_nfs
    bash -x $BaseDir/db_migrate.sh
    ###if supervisor service started, we need use "supervisorctl update" to start new conf####
    supervisorctl update
}

init_app

# Start
bash -x $BaseDir/start.sh
