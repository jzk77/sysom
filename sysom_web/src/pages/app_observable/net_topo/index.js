import GrafanaWrap from '../../Monitor/grafana'
/**
 * 应用观测网络拓扑图
 * @returns 
 */
const AppObservableNetTopology = (props) => {
    const queryParams = !!props.queryParams ? props.queryParams : {};
    // queryParams to query string
    const queryStr = Object.keys(queryParams).map(key => key + '=' + queryParams[key]).join('&').trim();
    let targetUrl = '/grafana/d/H04tHN34k/ntopo'
    if (queryStr.length > 0) {
        targetUrl += `?${queryStr}`
    }
    return (
        <div>
            <GrafanaWrap src={targetUrl} />
        </div>
    )
};
export default AppObservableNetTopology;