import WebConsole from '@/components/WebConsole';
import { useIntl, FormattedMessage } from 'umi';

const VmcoreAnalyse = (props) => {
  const intl = useIntl();
  const userId = localStorage.getItem('userId');
  return (
    <WebConsole title={intl.formatMessage({
      id: 'pages.vmcore.vmcoreonlineanalysis',
      defaultMessage: 'Vmcore online analysis',
    })} kernel_version={props.location.query.kernel_version} vmcore_file = {props.location.query.vmcore_file}  user_id={userId} />
  )
};

export default VmcoreAnalyse
