/*
 * @Author: wb-msm241621
 * @Date: 2023-07-21 15:23:44
 * @LastEditTime: 2023-07-27 16:27:12
 * @Description: 
 */
import { useState } from 'react'
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Spin } from 'antd';
import { JsonView, allExpanded, darkStyles, defaultStyles } from 'react-json-view-lite';
import { useIntl, FormattedMessage } from 'umi';
import { getNodeLogList, rootCauseAnalysis } from '../service'

// const { Table } = ProTable

const NodeList = () => {
    const intl = useIntl();
    const [loading, setLoading] = useState(false)
    const columns = [
        {
            title: <FormattedMessage id="pages.journal.node.instance" defaultMessage="instance" />,
            dataIndex: 'instance',
            valueType: 'textarea',
            copyable: true,
        },
        {
            title: <FormattedMessage id="pages.journal.node.event_type" defaultMessage="event_type" />,
            dataIndex: 'event_type',
            filters: true,
            // onFilter: true,
            hideInSearch: true,
            ellipsis: true,
            valueEnum: {
                metric_exception: { text: 'MetricException' },
                log_exception: { text: 'LogException' },
            }
        },
        {
            title: <FormattedMessage id="pages.journal.node.ts" defaultMessage="ts" />,
            dataIndex: 'ts',
            valueType: 'textarea',
            hideInSearch: true,
            sorter: (a, b) => a.ts - b.ts,
        },
        {
            title: <FormattedMessage id="pages.journal.node.description" defaultMessage="description" />,
            dataIndex: 'description',
            valueType: 'textarea',
            tip: '详情过长会自动收缩',
            ellipsis: true,
            hideInSearch: true,
        },
        {
            title: <FormattedMessage id="pages.journal.node.option" defaultMessage="Operating" />,
            key: 'option',
            dataIndex: 'option',
            valueType: 'option',
            render: (_, record) => [
                <span key='root_cause_analysis'>
                    {
                        record.event_type === "metric_exception" ?
                            (
                                loading ?
                                    <Spin size='small' />
                                    :
                                    <a onClick={() => {
                                        let extraData = record.extra;
                                        setLoading(true);
                                        rootCauseAnalysis({
                                            "rca_type": "rca",
                                            "machine_ip": record.instance,
                                            "base_item": extraData?.metrics,
                                            "time": extraData?.ts
                                        })
                                            .then(res => {
                                                setLoading(false);
                                                window.open(res);
                                            })
                                            .catch(err => {
                                                console.log(err);
                                                setLoading(false);
                                            })
                                    }}>
                                        <FormattedMessage id="pages.journal.node.root_cause_analysis" defaultMessage="Root cacuse analysis" />
                                    </a>
                            )
                            :
                            ""
                    }
                </span>,
            ],
        }
    ]
    return (
        <PageContainer>
            <ProTable headerTitle={intl.formatMessage({
                id: 'pages.journal.node.title',
                defaultMessage: 'Audit',
            })}
                rowKey="id"
                search={{
                    labelWidth: 120,
                }}
                toolBarRender={() => [
                ]}
                pagination={{ showSizeChanger: true, pageSizeOptions: [10, 20], defaultPageSize: 10 }}
                request={async (params, sort, filter) => {
                    console.log(filter)
                    if (Object.keys(sort).length != 0) {
                        console.log(sort.ts)
                        const _sort = sort.ts === "ascend" ? "ts" : "-ts"
                        params['sort'] = _sort
                    }
                    if (filter.event_type) {
                        params['event_type'] = filter.event_type.join(",")
                    }
                    const result = await getNodeLogList(params)
                    return result
                }}
                columns={columns}
                expandable={{
                    expandedRowRender: (record) => (
                        <p><JsonView data={record.extra} shouldExpandNode={allExpanded} style={{
                            ...darkStyles,
                            container: ""
                        }} /></p>
                    ),
                    showExpandColumn: true,
                }}
            />
        </PageContainer>
    )
}

export default NodeList;
