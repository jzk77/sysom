import { ModalForm, ProFormTextArea, ProFormDigit, ProFormText, ProFormSelect } from '@ant-design/pro-form';
import { forwardRef } from 'react';
import * as PropTypes from 'prop-types';
import { useIntl, FormattedMessage } from 'umi';

/**
 * 诊断离线导入模态框
 * @param {*} props 
 *      props.visible => Whether current modal visible
 *      props.title => 模态框顶部的标题  
 *      props.modalWidth => 模态框的宽度，默认为 440px
 *      props.onVisibleChange => Invoke while visible change
 *      props.onFinish => Import successfully
 */
let OfflineImportModal = (props, ref) => {
    const {
        title,
        visible,
        modalWidth,
        onVisibleChange,
        onFinish,
        taskForm,
        serviceName,
        queryParams,
    } = props;

    const intl = useIntl();

    return (
        <ModalForm
            style={{
                margin: "auto"
            }}
            modalProps={{
                centered: true,
                bodyStyle: {
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column"
                }
            }}
            title={title}
            width={modalWidth}
            visible={visible}
            onVisibleChange={onVisibleChange}
            onFinish={value => {
                onFinish({
                    ...value
                })
            }}
        >

            <ProFormText
                name={"service_name"}
                initialValue={serviceName}
                hidden={true}
            />
            {
                taskForm?.map(formItem => {
                    let value = formItem.name in queryParams ? queryParams[formItem.name] : ""
                    if (formItem.type == "text" || formItem.type == "select_host") {
                        return (< ProFormText
                            key={formItem.name}
                            name={formItem.name}
                            label={formItem.label}
                            value={value}
                            initialValue={!!value ? value : formItem.initialValue}
                            tooltip={formItem.tooltips}
                            disabled={!!formItem.disabled}
                        />);
                    } else if (formItem.type == "digit") {
                        return (< ProFormDigit
                            key={formItem.name}
                            name={formItem.name}
                            label={formItem.label}
                            value={value}
                            initialValue={!!value ? value : formItem.initialValue}
                            tooltip={formItem.tooltips}
                            disabled={!!formItem.disabled}
                        />);
                    } else if (formItem.type == "select") {
                        return (
                            < ProFormSelect
                                key={formItem.name}
                                name={formItem.name}
                                label={formItem.label}
                                value={value}
                                initialValue={!!value ? value : formItem.initialValue}
                                tooltip={formItem.tooltips}
                                options={formItem.options}
                                disabled={!!formItem.disabled}
                            />);
                    }
                })
            }
            <ProFormTextArea
                label={
                    intl.formatMessage({
                        id: 'pages.diagnose.offline_log',
                        defaultMessage: 'Offline log',
                    })
                }
                width="md"
                name="offline_log"
                tooltip={
                    intl.formatMessage({
                        id: 'pages.diagnose.offline_import.offline_log_tip',
                        defaultMessage: 'The result of the execution of the diagnosis command on the node side',
                    })
                }
                allowClear
            />
        </ModalForm>
    )
}

OfflineImportModal = forwardRef(OfflineImportModal);

OfflineImportModal.displayName = "OfflineImportModal";

OfflineImportModal.propTypes = {
    title: PropTypes.string,
    visible: PropTypes.bool,
    modalWidth: PropTypes.string,
    onVisibleChange: PropTypes.func,
    onFinish: PropTypes.func
}

// Props 参数默认值
OfflineImportModal.defaultProps = {
    title: <FormattedMessage id="pages.diagnose.importdiagnosisresultsoffline" defaultMessage="Import diagnosis results offline" />,
    visible: false,
    modalWidth: "440px",
    onVisibleChange: () => { },
    onFinish: () => { },
}

export default OfflineImportModal;