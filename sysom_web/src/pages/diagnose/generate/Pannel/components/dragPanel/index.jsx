import { useDrag } from 'react-dnd';
import styles from './index.less'
import { getLocale } from "umi";
import { componentConfigs } from "@/pages/diagnose/generate/Pannel/component-config";
import { useEffect } from 'react';
import { message } from 'antd';
import { usePanelGeneratorStore } from "@/pages/diagnose/generate/Pannel/store/panelGeneratorStore";

/**
 * anything inside this component will be draggable
 * @param containerType 
 * @param type component type
 * @returns {JSX.Element} draggable component
 * @constructor DragPanel
 */
function DragPanel({ setIsDragging, type, container }) {
    let panelConfig = componentConfigs[type]
    if (panelConfig === undefined)
        panelConfig = componentConfigs[type]
    const lang = getLocale()
    const configStore = usePanelGeneratorStore((state) => state.data)
    const setIsDraggingNow = usePanelGeneratorStore((state) => state.setIsDraggingNow)
    const setConfigStore = usePanelGeneratorStore((state) => state.setConfigStore)
    const [{ opacity, isDragging }, drag] = useDrag(() => ({
        type: type,
        item: { name: type },
        collect: (monitor) => ({
            opacity: monitor.isDragging() ? 0.4 : 1,
            isDragging: monitor.isDragging() ? 1 : 0,
        }),
        end(item, monitor) {
            const dropResult = monitor.getDropResult()
            console.log(dropResult, monitor.getTargetIds())
            if (dropResult?.type === 'row') {
                if (type === 'row') {
                    message.error('不支持嵌套行组件')
                } else {
                    if (item && dropResult) {
                        const currentPanel = configStore.pannels
                        const currentRow = currentPanel.find((item) => item?.key === dropResult.key)
                        if (currentRow.children.length >= 3) {
                            message.error('每行最多只能放置3个组件')
                        }
                        else {
                            currentRow.children.push({ ...JSON.parse(JSON.stringify(componentConfigs[type].config)), key: Math.random().toString(36).substring(2, 8) })
                            setConfigStore({ ...configStore, pannels: currentPanel })
                        }
                    }
                }
            } else {
                if (item && dropResult) {
                    if (container === 'taskform') {
                        const currentForm = configStore.taskform
                        currentForm.push({ ...JSON.parse(JSON.stringify(componentConfigs[type].config)), key: Math.random().toString(36).substring(2, 8) })
                        setConfigStore({ ...configStore, taskform: currentForm })
                    } else if (container === 'panels') {
                        const currentPanel = configStore.pannels
                        currentPanel.push({ ...JSON.parse(JSON.stringify(componentConfigs[type].config)), key: Math.random().toString(36).substring(2, 8) })
                        setConfigStore({ ...configStore, pannels: currentPanel })
                    }
                }
            }
        },
    }), [configStore])
    useEffect(() => {
        setIsDragging(isDragging)
        setIsDraggingNow(isDragging)
    }, [isDragging])
    return (
        <div ref={drag} style={{ opacity }} className={styles.component}>
            <div className={`${styles.dragBlock}`} style={{ background: `url(${panelConfig.icon})`, backgroundSize: 'cover' }}></div>
            {
                lang === 'en-US' ? panelConfig.enName : panelConfig.name
            }
        </div>
    );
}

export default DragPanel;
