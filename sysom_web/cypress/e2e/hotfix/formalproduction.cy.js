/// <reference types="cypress" />

/**
 * This test is used for hotfix build test.
 * It test hotfix build, convert to formal
 * wait, refresh state function.
 * Once the hotfix build success, change to 
 * formal hotfix list for futher testing.
 */
it("login success", function() {
	cy.login()
	
	cy.intercept("POST", "api/v1/hotfix/upload_patch/")
		.as("upload_patch")


	cy.intercept("POST", "api/v1/hotfix/create_hotfix/")
	.as("create")

	cy.intercept("GET", "api/v1/hotfix/get_hotfix_list/?*")
	.as("gethotfixtasks")

	cy.intercept("POST", "api/v1/hotfix/set_formal/")
		.as("set_formal")

	cy.intercept("DELETE", "api/v1/hotfix/delete_hotfix/")
		.as("delete_hotfix")

	const getAndCheckTaskResult = (task_id) => {
		// 1.点击一下刷新按钮
		cy.get('span[aria-label="reload"]').click()
		cy.wait('@gethotfixtasks').its("response.statusCode").should("eq", 200)
		cy.wait(100)
		cy.get(".ant-table-tbody > tr:nth-child(1)").then($el => {
				let current_text = $el.text()
				if (current_text.indexOf("等待构建") != -1  || current_text.indexOf("正在构建") != -1) {
					//2.诊断运行中，等待一分钟后再次检查
					cy.wait(1000*60)
					getAndCheckTaskResult(task_id)
				} else {
					if (current_text.indexOf("构建成功") != -1) {
						
						cy.get(".ant-table-tbody > tr:nth-child(1) > td:nth-child(8)").click()

						//3.转正式包的确认
						cy.get(".ant-popover-content").first().within(() => {
							cy.get("div.ant-popover-buttons > button.ant-btn.ant-btn-primary.ant-btn-sm").first().click()
						})
						
						//4.转正式包接口结果判断
						cy.wait('@set_formal')
							.then((interception) => {
								cy.wrap({
									statusCode: interception.response?.statusCode
								}).its("statusCode").should("eq", 200)
							})
				
						
						//5.点击下载按钮
						cy.get(".ant-table-tbody > tr:nth-child(1) > td:nth-child(9) > div").click()

						// 7. 构建任务成功以后，前往正式热补丁列表执行操作
						cy.visit('/hotfix/formal_hotfix');
						cy.get('#created_at').click();
						cy.get('.ant-picker-today-btn').click();
						//cy.get(':nth-child(1) > .ant-btn > span').click({multiple: true});
						cy.get('#hotfix_name').clear();
						cy.get('#hotfix_name').type('test');
						cy.get(':nth-child(2) > .ant-btn > span').click();
						cy.get('.ant-btn > .anticon > svg').click({multiple: true});
						cy.get('.ant-space > :nth-child(2) > span > a').click();

						// 访问热补丁制作页面
						cy.visit("/hotfix/make")
						
				
						//6.点击第一条数据删除
						cy.get('table').within(() => {
							cy.get('tr').eq(1).contains("删除").click();
						})
				
						//7.点击确认删除
						cy.get(".ant-popover-buttons > button.ant-btn.ant-btn-primary.ant-btn-sm").last().click()
						
				
						//8.删除接口结果判断
						cy.wait('@delete_hotfix')
							.then((interception) => {
								cy.wrap({
									statusCode: interception.response?.statusCode
								}).its("statusCode").should("eq", 200)
							})
						}
				   
				}
			})
	}

	// 1. 访问热补丁中心构建页面
	cy.visit("/hotfix/make")

	//2. 填入参数
	cy.get('#kernel_version').type('5.10.112-11.1.al8.x86_64');
	cy.get('#hotfix_name').type('test');

	//3.上传文件
	cy.get("#patch").selectFile('cypress/e2e/hotfix/5.10-new-globals.patch',{force: true})


	//4.文件结果判断
	cy.wait('@upload_patch')
		.then((interception) => {
			cy.wrap({
				statusCode: interception.response?.statusCode
			}).its("statusCode").should("eq", 200)
		})
	
	//5.点击创建
	cy.get("#root > div > section > div > main > div > div.ant-pro-grid-content > div > div > div.ant-pro-page-container-children-content > div > div.ant-pro-card.ant-pro-table-search.ant-pro-table-search-query-filter > form > div > div.ant-col.ant-col-8.ant-col-offset-16 > div > div > div.ant-col.ant-form-item-control > div > div > div > div > div > div:nth-child(2) > button > span").click()
	
	//6.创建任务触发构建判断
	cy.wait('@create')
		.then((interception) => {
			cy.wrap({
				statusCode: interception.response?.statusCode
			}).its("statusCode").should("eq", 200)
			expect(interception.response.body.data).to.have.property("id")

			// 得到构建 ID
			let id = interception.response?.body.data.id
			getAndCheckTaskResult(id)

		})

})
