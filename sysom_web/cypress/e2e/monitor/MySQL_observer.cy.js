/// <reference types="cypress" />

describe("SysOM MySQL Observer Dashboard Test", () => {
    beforeEach(() => {
        cy.login();
    })

    it("MySQL Observer test", () => {
        // 1. 访问集群监控页面
        cy.visit("app_observable/mysql");

        // 2. 等待页面加载完成
        cy.wait(2000);
    
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL Error Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL Slow_Sql Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL Net_Drops Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL OOM Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL RT Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL Sched_Delay Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("MySQL Long_Time_D Alarm");
        cy.getPannelContentByTitle("异常告警分布（次数）").contains("Mysql CPU_High Alarm");

        cy.getPannelContentByTitle("mySQL 连接线程池使用").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {

            }
        })

        cy.getPannelContentByTitle("MySQL CPU占用率").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL CPU占用率").find("tbody tr").eq(0).find("td").eq(0).contains("user");
        cy.getPannelContentByTitle("MySQL CPU占用率").find("tbody tr").eq(1).find("td").eq(0).contains("sys");
        cy.getPannelContentByTitle("MySQL CPU占用率").find("tbody tr").eq(2).find("td").eq(0).contains("total");

        cy.getPannelContentByTitle("MySQL CPU让出率").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL CPU让出率").find("tbody tr").eq(0).find("td").eq(0).contains("让出率");



        cy.getPannelContentByTitle("MySQL Undolog链表长度&长事务").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {

            }
        })
        cy.getPannelContentByTitle("MySQL 内存缓存池使用").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {

            }
        })
        cy.getPannelContentByTitle("MySQL OS内存使用分布").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列
        cy.getPannelContentByTitle("MySQL Redolog使用量").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {

            }
        })
        cy.getPannelContentByTitle("MySQL RT").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL RT").find("tbody tr").eq(0).find("td").eq(0).contains("Avg");

        cy.getPannelContentByTitle("MySQL请求详情").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {

            }
        })

        cy.getPannelContentByTitle("MySQL 等待IO资源延迟(平均每秒)").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL 等待IO资源延迟(平均每秒)").find("tbody tr").eq(0).find("td").eq(0).contains("iowait");

        cy.getPannelContentByTitle("MySQL申请OS内存延迟").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL申请OS内存延迟").find("tbody tr").eq(0).find("td").eq(0).contains("memAllocDelay");

        cy.getPannelContentByTitle("MySQL OS调度延迟").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL OS调度延迟").find("tbody tr").eq(0).find("td").eq(0).contains("调度延迟");

        cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").should("have.length.gte", 1);
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").eq(0).find("td").eq(0).contains("MySQL.Total_delay");
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").eq(1).find("td").eq(0).contains("MySQL.Disk_delay");
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").eq(2).find("td").eq(0).contains("MySQL.OS_delay_by_io_block");
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").eq(3).find("td").eq(0).contains("MySQL.OS_delay_by_disk_driver");
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").eq(4).find("td").eq(0).contains("MySQL.OS_delay_by_io_complete");
                cy.getPannelContentByTitle("MySQL数据IO处理延迟分布").find("tbody tr").eq(5).find("td").eq(0).contains("MySQL.OS_delay_by_io_done");
            }
        })

        cy.getPannelContentByTitle("MySQL 磁盘各队列级IO延迟分布").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
                cy.getPannelContentByTitle("MySQL 磁盘各队列级IO延迟分布").find("tbody tr").should("have.length.gte", 1);
                cy.getPannelContentByTitle("MySQL 磁盘各队列级IO延迟分布").find("tbody tr").eq(0).find("td").eq(0).contains(/[a-z]+\.Qid[0-9]+\.Total_delay/);
                cy.getPannelContentByTitle("MySQL 磁盘各队列级IO延迟分布").find("tbody tr").eq(1).find("td").eq(0).contains(/[a-z]+\.Qid[0-9]+\.Disk_delay/);
                cy.getPannelContentByTitle("MySQL 磁盘各队列级IO延迟分布").find("tbody tr").eq(2).find("td").eq(0).contains(/[a-z]+\.Qid[0-9]+\.OS_delay/);
            }
        })

        cy.getPannelContentByTitle("MySQL 磁盘级IO延迟分布").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
                cy.getPannelContentByTitle("MySQL 磁盘级IO延迟分布").find("tbody tr").should("have.length.gte", 1);
                cy.getPannelContentByTitle("MySQL 磁盘级IO延迟分布").find("tbody tr").eq(0).find("td").eq(0).contains(/[a-z]+\.Total_delay/);
                cy.getPannelContentByTitle("MySQL 磁盘级IO延迟分布").find("tbody tr").eq(1).find("td").eq(0).contains(/[a-z]+\.Disk_delay/);
                cy.getPannelContentByTitle("MySQL 磁盘级IO延迟分布").find("tbody tr").eq(2).find("td").eq(0).contains(/[a-z]+\.OS_delay/);
            }
        })

        cy.getPannelContentByTitle("MySQL每CPU生产IO请求热力分布").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
            }
        })

        cy.getPannelContentByTitle("MySQL每CPU处理IO中断热力分布").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
            }
        })

        cy.getPannelContentByTitle("MySQL IO吞吐").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL IO吞吐").find("tbody tr").eq(0).find("td").eq(0).contains("rBPS");
        cy.getPannelContentByTitle("MySQL IO吞吐").find("tbody tr").eq(1).find("td").eq(0).contains("wBPS");

        cy.getPannelContentByTitle("MySQL OS脏页量").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL OS脏页量").find("tbody tr").eq(0).find("td").eq(0).contains("Dirty Pages");
        cy.getPannelContentByTitle("MySQL OS脏页量").find("tbody tr").eq(1).find("td").eq(0).contains("Dirty Thresh");

        cy.getPannelContentByTitle("MySQL网络吞吐").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL网络吞吐").find("tbody tr").eq(0).find("td").eq(0).contains("netRecTraffic");
        cy.getPannelContentByTitle("MySQL网络吞吐").find("tbody tr").eq(1).find("td").eq(0).contains("netSendTraffic");

        cy.getPannelContentByTitle("MySQL请求数").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("MySQL请求数").find("tbody tr").eq(0).find("td").eq(0).contains("mysql requestCnt");
    })
})