/// <reference types="cypress" />

describe("SysOM Cluster Monitor Dashboard Test", () => {
    beforeEach(() => {
        cy.login();
    })

    it("Cluster monitor test", () => {
        // 1. 访问集群监控页面
        cy.visit("/monitor/cluster_monitor");

        // 2. 等待页面加载完成
        cy.wait(2000);

        // assert health score below 100
        cy.getPannelContentByTitle("Cluster Health").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0);
                })
            }
        })

        cy.getPannelContentByTitle("Errors Health").then(($el) => {
            if ($el.text().includes("N/A")) {
                cy.wrap($el).contains("N/A");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0);
                })
            }
        })

        cy.getPannelContentByTitle("Latency Health").then(($el) => {
            if ($el.text().includes("N/A")) {
                cy.wrap($el).contains("N/A");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0);
                })
            }
        })

        cy.getPannelContentByTitle("Load(Traffic) Health").then(($el) => {
            if ($el.text().includes("N/A")) {
                cy.wrap($el).contains("N/A");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0)
                })
            }
        })
    
        cy.getPannelContentByTitle("集群总CPU核数/节点数").contains("cores");
        cy.getPannelContentByTitle("集群总CPU核数/节点数").contains("nodes");

        cy.getPannelContentByTitle("集群CPU利用率分布").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列

        cy.getPannelContentByTitle("集群内存使用分布").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列

        cy.getPannelContentByTitle("集群内存总量").contains(/\d+/).then(($el) => {
            const num = parseInt($el.text());
            expect(num).to.be.least(0);
        })

        cy.getPannelContentByTitle("节点内存延时诊断").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {

            }
        })

        cy.getPannelContentByTitle("容器内存延时诊断").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
                
            }
        })

        cy.getPannelContentByTitle("节点内存使用率诊断").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列

        cy.getPannelContentByTitle("节点CPU利用率诊断").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列

        cy.getPannelContentByTitle("节点CPU延时诊断").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列

        cy.getPannelContentByTitle("集群平均 CPU利用率").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列

        cy.getPannelContentByTitle("集群平均内存使用率").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列
        
        cy.getPannelContentByTitle("集群平均CPU利用率分布情况").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("集群平均CPU利用率分布情况").find("tbody tr").eq(0).find("td").eq(0).contains("user");
        cy.getPannelContentByTitle("集群平均CPU利用率分布情况").find("tbody tr").eq(1).find("td").eq(0).contains("nice");
        cy.getPannelContentByTitle("集群平均CPU利用率分布情况").find("tbody tr").eq(2).find("td").eq(0).contains("sys");
        cy.getPannelContentByTitle("集群平均CPU利用率分布情况").find("tbody tr").eq(3).find("td").eq(0).contains("softirq");
        cy.getPannelContentByTitle("集群平均CPU利用率分布情况").find("tbody tr").eq(4).find("td").eq(0).contains("iowait");

        cy.getPannelContentByTitle("总内存使用情况").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("总内存使用情况").find("tbody tr").eq(0).find("td").eq(0).contains("free");
        cy.getPannelContentByTitle("总内存使用情况").find("tbody tr").eq(1).find("td").eq(0).contains("used");
        cy.getPannelContentByTitle("总内存使用情况").find("tbody tr").eq(2).find("td").eq(0).contains("cache+buffer");
        cy.getPannelContentByTitle("总内存使用情况").find("tbody tr").eq(3).find("td").eq(0).contains("total");

        cy.getPannelContentByTitle("集群平均调度延迟").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("集群平均调度延迟").find("tbody tr").eq(0).find("td").eq(0).contains("调度延迟");

        cy.getPannelContentByTitle("集群用户态内存使用情况").find("ul li").should("have.length.gte", 1);
        
        cy.getPannelContentByTitle("集群节点平均load1").find("tbody tr").should("have.length.gte", 1);

        cy.getPannelContentByTitle("集群内核态内存使用情况").find("ul li").should("have.length.gte", 1);

        cy.getPannelContentByTitle("集群节点任务统计信息").find("tbody tr").should("have.length.gte", 1);
        cy.getPannelContentByTitle("集群节点任务统计信息").find("tbody tr").eq(0).find("td").eq(0).contains("nr_forks");
        cy.getPannelContentByTitle("集群节点任务统计信息").find("tbody tr").eq(1).find("td").eq(0).contains("nr_blocked");

        cy.getPannelContentByTitle("集群app与kernel内存对比").find("ul li").should("have.length.gte", 1);

        cy.getPannelContentByTitle("容器大盘").contains("容器大盘详情");

        cy.getPannelContentByTitle("节点大盘").contains("节点大盘详情");
    })
})