/// <reference types="cypress" />

describe("SysOM Container Monitor Dashboard Test", () => {
    beforeEach(() => {
        cy.login();
    })

    it("Container Monitor", () => {
        cy.wait(2000)
        
        // 1. 访问集群监控页面
        cy.visit("/monitor/container_monitor");

        // 2. 等待页面加载完成
        cy.wait(2000)

        /*
         * Pod Health
         */

        // assert health score below 100
        cy.getPannelContentByTitle("Pod Health").then(($el) => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0);
                })
            }
        })

        cy.getPannelContentByTitle("Errors Health").then(($el) => {
            if ($el.text().includes("N/A")) {
                cy.wrap($el).contains("N/A");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0);
                })
            }
        })

        cy.getPannelContentByTitle("Latency Health").then(($el) => {
            if ($el.text().includes("N/A")) {
                cy.wrap($el).contains("N/A");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0);
                })
            }
        })

        cy.getPannelContentByTitle("Load(Traffic) Health").then(($el) => {
            if ($el.text().includes("N/A")) {
                cy.wrap($el).contains("N/A");
            } else {
                cy.wrap($el).contains(/\d+/).then(($el) => {
                    const num = parseInt($el.text());
                    expect(num).to.be.lte(100);
                    expect(num).to.be.gte(0)
                })
            }
        })

        /*
         * Pod Memory Monitor
         */
        cy.openMainLabel("Pod Memory Monitor")
        cy.openMainLabel("Pod Memory Monitor")
        
        // 当前面板Pod Cache Usage
        cy.panelAtagValueGteOrNoDataTest("Pod Memory Usage (top 5)")

        // 当前面板Pod Cache Usage
        cy.panelAtagValueGteOrNoDataTest("Pod Cache Usage (top 5)")

        // 当前面板Pod Mem Stat
        cy.panelAtagValueGteOrNoDataTest("Pod Mem Stat")
        
        // 当前面板Pod Cached File
        cy.panelAtagValueGteOrNoDataTest("Pod Cached File (top 5)")
        
        // 当前面板Pod Mem Event
        cy.panelAtagValueGteOrNoDataTest("Pod Mem Event")

        // 当前面板Pod Memory Rate
        cy.panelAtagValueGteOrNoDataTest("Memory Rate")

        // 当前面板Memory Global Direct Reclaim Latency
        cy.panelAtagValueGteOrNoDataTest("Memory Global Direct Reclaim Latency")

        // 当前面板Memory Direct Reclaim Latency
        cy.panelAtagValueGteOrNoDataTest("Memory Direct Reclaim Latency")

        // 当前面板Memory Compact Latency
        cy.panelAtagValueGteOrNoDataTest("Memory Compact Latency")

        /*
         * Pod CPU Monitor
         */

        cy.openMainLabel("Pod CPU Monitor")
        cy.openMainLabel("Pod CPU Monitor")
        
        // 当前面板Pod CPU Usage
        cy.panelAtagValueGteOrNoDataTest("Pod CPU Usage")

        // 当前面板Pod CPU nr_throttled
        cy.panelAtagValueGteOrNoDataTest("Pod CPU nr_throttled")

        // 当前面板Pod wait_latency
        cy.panelAtagValueGteOrNoDataTest("Pod wait_latency")
        
        // 当前面板Pod cfs_quota
        cy.panelAtagValueGteOrNoDataTest("Pod cfs_quota")

        /*
         * Pod Network Monitor
         */

        cy.openMainLabel("Pod Network Monitor")
        cy.openMainLabel("Pod Network Monitor")
        
        // 当前面板Pod Network Traffic by Bytes
        cy.panelAtagValueGteOrNoDataTest("Pod Network Traffic by Bytes")

        // 当前面板Pod Network Traffic by Packets
        cy.panelAtagValueGteOrNoDataTest("Pod Network Traffic by Packets")

        // 当前面板Pod Network Traffic Drop
        cy.panelAtagValueGteOrNoDataTest("Pod Network Traffic Drop")        

        /*
         * Pod IO Monitor
         */

        cy.openMainLabel("Pod IO Monitor")
        cy.openMainLabel("Pod IO Monitor")
        
        // 当前面板Pod Writes/Reads Bytes Rates
        cy.panelAtagValueGteOrNoDataTest("Pod Writes/Reads Bytes Rates");
    
        // 当前面板Pod Writes/Reads IOs Rates
        cy.panelAtagValueGteOrNoDataTest("Pod Writes/Reads IOs Rates");

        // 当前面板Pod IO Queued
        cy.panelAtagValueGteOrNoDataTest("Pod IO Queued");

        // 当前面板Pod IO Wait Time
        cy.panelAtagValueGteOrNoDataTest("Pod IO Wait Time");       
    })

})