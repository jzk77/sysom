/// <reference types="cypress" />

describe("SysOM Host Manager Test", () => {
    beforeEach(() => {
        cy.login()
    })
    it("Crate and delete host", () => {
        cy.intercept("GET", "/api/v1/host/")
            .as("getHostList")
        cy.intercept("POST", "/api/v1/host/")
            .as("createHost")

        cy.intercept("DELETE", "/api/v1/host/*")
            .as("deleteHost")

        // 1. 访问主机列表也米娜
        cy.visit("/host/list")

        cy.wait('@getHostList', { timeout: 10000 })
            .then((interception) => {
                expect(interception).to.have.property('response')
                expect(interception.response?.body.code, 'code').to.equal(200)
                expect(interception.response.statusCode).to.equal(200)

                const { data } = interception.response.body
                const ipList = data.map((item) => { return item.ip })
                const defaultHostIpOne = Cypress.env("HOSTS")[0]

                // 判断主机列表是否已存在默认测试主机
                if (ipList.includes(defaultHostIpOne)) {

                    // 找到默认的主机，并点击删除按钮
                    cy.get("td")
                        .contains(defaultHostIpOne)
                        .parent()
                        .within(() => {
                            cy.get("td").contains("删除").click()
                        })

                    // 点击删除按钮之后需要在弹出的浮窗中点击OK确认
                    cy.get(".ant-popover-buttons").find("button").contains("OK").click()
                    // 确认删除接口调用结果为 200
                    cy.wait('@deleteHost')
                        .then((interception) => {
                            cy.wrap({
                                statusCode: interception.response?.statusCode
                            }).its("statusCode").should("eq", 200)
                        })
                }
            })

        // 2. 点击新建主机并开始添加
        cy.addDefaultHost()

        // 创建完主机后等待一秒钟，一秒钟后执行删除操作
        cy.wait(1000)
        

        // 找到最新创建的主机，并点击删除按钮
        cy.get("td")
            .contains("127.0.0.1")
            .parent()
            .within(() => {
                cy.get("td").contains("删除").click()
            })
        
        // 点击删除按钮之后需要在弹出的浮窗中点击OK确认
        cy.get(".ant-popover-buttons").find("button").contains("OK").click()
        // 确认删除接口调用结果为 200
        cy.wait('@deleteHost')
            .then((interception) => {
                cy.wrap({
                    statusCode: interception.response?.statusCode
                }).its("statusCode").should("eq", 200)
            })

    })
})