/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- iofsstat", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke iofsstat diagnosis, and check result", () => {
        //cy.wait(5000)
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/storage/iofsstat",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
                "timeout": "5",
            },
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                // cy.get('.ant-statistic-content-value').click()
                cy.get(':nth-child(1) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "diagnose result");
                cy.get(':nth-child(2) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Disk IO traffic analysis");
                cy.get(':nth-child(3) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Process IO traffic analysis");
                cy.get(':nth-child(4) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Process IO Block Size Distribution");
                //cy.get('.ant-statistic-content-value').should("contain.text", "total")
                /* ==== End Cypress Studio ==== */
            })
    })
})