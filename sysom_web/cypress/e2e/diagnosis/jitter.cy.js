/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- jitter", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke jitter diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/net/jitter",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
            },

            // "result": {
            //     "senderflow": {
            //         "data": [
            //             {
            //                 "key": "send",
            //                 "title": "kernelSnd",
            //                 "text": "MAX:0ms AVG:0ms MIN:0ms"
            //             },
            //             {
            //                 "key": "out",
            //                 "title": "externalLink",
            //                 "text": "MAX:0ms AVG:0ms MIN:0ms"
            //             },
            //             {
            //                 "key": "recv",
            //                 "title": "kernelRcv",
            //                 "text": "MAX:0ms AVG:0ms MIN:0ms"
            //             }
            //         ]
            //     },
            //     "receiverflow": {
            //         "data": [
            //             {
            //                 "key": "recv",
            //                 "title": "kernelRcv",
            //                 "text": "MAX:0ms AVG:0ms MIN:0ms"
            //             },
            //             {
            //                 "key": "send",
            //                 "title": "kernelSnd",
            //                 "text": "MAX:0ms AVG:0ms MIN:0ms"
            //             }
            //         ]
            //     },
            //     "summary": "Receiver: kernelRcv-(0, 0, 0), kernelSnd-(0, 0, 0)"
            // },
            (result) => {
                // cy.get(':nth-child(1) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "发送端报文路径");
                /* ==== Generated with Cypress Studio ==== */
                if (result.status === "Success") {
                    cy.get(':nth-child(1) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "发送端报文路径");
                    cy.get(':nth-child(2) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "接收端报文路径");
                    /* ==== End Cypress Studio ==== */
                }
            })
    })
})
