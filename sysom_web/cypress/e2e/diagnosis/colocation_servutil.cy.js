/// <reference types="cypress" />

describe("SysOM Cluster Manager Test", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke colocation cpi diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/colocation/serveutil",

            // 诊断参数
            {
                "instance": "127.0.0.1",
                "moment": "2024-03-05 14:15:49",
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            (result) => {
		cy.get('.ant-pro-card').eq(3).contains('诊断结果汇总')
		cy.get('.ant-pro-card').eq(3).find(".ant-pro-card-body").contains('结论')
		cy.get('.ant-pro-card').eq(3).find(".ant-pro-card-body").contains('修复建议')
		cy.get('.ant-pro-card').eq(4).contains('受干扰容器详情')
              //  cy.get('.ant-pro-card-border.ant-pro-card-contain-card > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Event overview");
            })
    })
})
