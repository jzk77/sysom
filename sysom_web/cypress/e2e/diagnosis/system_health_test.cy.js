/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- system health", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke system health diagnosis, and check result", () => {
        //cy.wait(5000)
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "diagnose/ossre",

            // 诊断参数
            {
                "instance": "127.0.0.1"
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            //待补充
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                cy.get('[data-row-key="CONFIG"] > .ant-table-cell-with-append').should("contain.text", "配置检查");
                cy.get('[data-row-key="SLI"] > .ant-table-cell-with-append').should("contain.text", "SLI检查");
                cy.get('[data-row-key="ISSUE"] > .ant-table-cell-with-append').should("contain.text", "已知问题检查");
                cy.get('[data-row-key="LOG"] > .ant-table-cell-with-append').should("contain.text", "日志检查");
                cy.get('[data-row-key="HW"] > .ant-table-cell-with-append').should("contain.text", "硬件检查");
                cy.get('[data-row-key="CONFIG"] > :nth-child(2) > .ant-badge > .ant-badge-status-text').invoke('text').should("match", /正常|告警/);
                cy.get('[data-row-key="SLI"] > :nth-child(2) > .ant-badge > .ant-badge-status-text').invoke('text').should("match", /正常|告警/);
                cy.get('[data-row-key="ISSUE"] > :nth-child(2) > .ant-badge > .ant-badge-status-text').invoke('text').should("match", /正常|告警/);
                cy.get('[data-row-key="LOG"] > :nth-child(2) > .ant-badge > .ant-badge-status-text').invoke('text').should("match", /正常|告警/);
                cy.get('[data-row-key="HW"] > :nth-child(2) > .ant-badge > .ant-badge-status-text').invoke('text').should("match", /正常|告警/);
                /* ==== End Cypress Studio ==== */
            })
    })
})
