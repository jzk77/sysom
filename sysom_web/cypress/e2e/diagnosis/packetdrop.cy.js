/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- packetdrop", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke packetdrop diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/net/packetdrop",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
            },

        // result {
        //     "packetDropSummary": {
        //         "data": [
        //             {
        //                 "key": "tcp",
        //                 "value": 94
        //             },
        //             {
        //                 "key": "udp",
        //                 "value": 0
        //             },
        //             {
        //                 "key": "icmp",
        //                 "value": 0
        //             },
        //             {
        //                 "key": "hardware",
        //                 "value": 0
        //             }
        //         ]
        //     },
        //     "packetDropAnalysis": {
        //         "data": []
        //     }
        // },

            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                cy.diagnosisTaskResultHandler(result, () => {
                    cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-title').should("contain.text", "tcp");
                    cy.get(':nth-child(3) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "丢包详情列表");
                })
                /* ==== End Cypress Studio ==== */
            })
    })
})
