#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))

clear_app() {
    # do nothing, don't erase nginx
    echo ""
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app