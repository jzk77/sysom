# -*- coding: utf-8 -*- #
"""
Time                2023/12/11 20:20
Author:             mingfeng (SunnyQjm), zhangque (Wardenjohn)
Email               mfeng@linux.alibaba.com, ydzhang@linux.alibaba.com
File                kafka_admin.py
Description:        Kafka Consumer. 
"""
import json
import uuid

from cec_base.consumer import Consumer
from cec_base.event import Event
from cec_base.url import CecUrl
from confluent_kafka import Consumer as ConfluentKafkaConsumer
from confluent_kafka import TopicPartition
from clogger import logger
from queue import Queue
from .common import StaticConst, ClientBase

class KafkaConsumer(Consumer, ClientBase):
    """A Kafka Based Consumer

    Description:
            KafkaConsumer._EVENT_KEY_KAFKA_CONSUMER_MESSAGE is an key.
            This key is use for internal message storage. An Event from consumer get from Kafka server
            will be put into the dicrionary in Event object. Use Event.get(key) can get the event object
            for futher operations.
    """

    _EVENT_KEY_KAFKA_CONSUMER_MESSAGE = "_EVENT_KEY_KAFKA_CONSUMER_MESSAGE"

    def __init__(self, url: CecUrl, topic_name: str, consumer_id: str = "",
                 group_id: str = "", start_from_now: bool = True,
                 default_batch_consume_limit: int = 5):
        super().__init__(topic_name, consumer_id, group_id, start_from_now, 
                         default_batch_consume_limit=default_batch_consume_limit)
        ClientBase.__init__(self, url)
        self._current_url = ""
        # save the original special params
        orig_special_params = url.params

        self._batch_consume_limit = self.get_special_param(
            StaticConst.KAFKA_SPECIAL_PARM_CEC_BATCH_CONSUMER_LIMIT
        )
        self._timeout = self.get_special_param(
            StaticConst.KAFKA_SPECIAL_PARM_CEC_CONSUME_TIMEOUT
        )

        # before connect to cec by url, filter the parameter kafka not support
        url.params = StaticConst.parse_kafka_connection_params(url.params)

        # set auto-offset-reset = earliest if you want to consume message
        # from the start
        if group_id != "" or not start_from_now:
            url.params['auto.offset.reset'] = 'earliest'
        else:
            url.params['auto.offset.reset'] = 'latest'
        
        url.params['client.id'] = consumer_id

        self._current_url = url
        self._kafka_consumer_client: ConfluentKafkaConsumer = None
        # if group_id is set, use the given group id or generate one random group id
        url.params['group.id'] = uuid.uuid4() if group_id == "" else group_id  
         
        self.connect_by_cec_url(url)
        self._kafka_consumer_client.subscribe([topic_name], on_assign=self.on_assign)
        self._last_event_id: str = None      # save the id of lastest consume message
        self._message_cache_queue = Queue()  # Queue of message cache 
        
    def on_assign(self, consumer: ConfluentKafkaConsumer,
                  partitions: [TopicPartition]):
        """on_assign

        Args:
            consumer (ConfluentKafkaConsumer): _description_
            partitions (TopicPartition]): _description_
        """
        print(partitions)
        pass
    
    def consume(self, timeout: int = -1, auto_ack: bool = False,
                batch_consume_limit: int = -1) -> [Event]:
        """consume

        Args:
            timeout (int, optional): timeout setting (ms). Defaults to -1. If timeout <=0 ,it means block to wait
            auto_ack (bool, optional): set to auto ack an message, which is effect to group consume. Defaults to False.
                1. If auto_ack switch to open, every event read by consumer will be ack automaticlly.
                2. The caller should make sure this event will be correctly handled
            batch_consume_limit (int, optional): The limitation of batch consume. Defaults to 0. For some cases of Kafka,
                if the message do not achieve the consume limit batch, the consume process will block and wait. 
            timeout (int, optional): The timeout limit for consumer to wait. timeout is set default 1. Becase in some cases of 
                Kafka, if the number of message do not reach the batch_consume_limit, the consume process will block. This 
                parameter should be set in association with batch_consume_limit.


        Returns:
            [Event]: _description_
        Description:

        """
        batch_consume_limit = self._batch_consume_limit if batch_consume_limit <= 0 else batch_consume_limit
        timeout = self._timeout if timeout <=0 else timeout
        kafka_message = self._kafka_consumer_client.consume(
            batch_consume_limit,
            timeout/1000
        )
        # message is a list, which contain Event objects
        message_list: [Event] = []
        for message in kafka_message:
            try:
                event = Event(json.loads(message.value().decode('utf-8')),
                            f"{message.partition()}-{message.offset()}")
            except Exception as e:
                # if exception is raised, it means the message is not in the json format(event not in dict format)
                event = Event(message.value().decode('utf-8'),
                              f"{message.partition()}-{message.offset()}")
            # event => Event: Object
            # event.put => event._cache: {key:_EVENT_KEY, value:message}
            event.put(KafkaConsumer._EVENT_KEY_KAFKA_CONSUMER_MESSAGE, message)
            message_list.append(event)
            if auto_ack:
                self._kafka_consumer_client.commit(message)
        return message_list
    
    def ack(self, event: Event) -> int:
        """Ack one event

        Args:
            event (Event): _description_

        Returns:
            int: _description_
        
        Description:
        """
        message = event.get(KafkaConsumer._EVENT_KEY_KAFKA_CONSUMER_MESSAGE)
        ret = False
        if message is not None:
            # this is an asynchronous commit
            self._kafka_consumer_client.commit(message)
            ret = True
        return ret

    def __getitem__(self, item):
        """get the next message from queue

        Args:
            item (_type_): _description_
        
        Description:
            overwite __getitem__ 
            get next message from _message_cache_queue.
            if _message_cache_queue is empty, try to consume event from server
            then put it into the _message_cache_queue for iterator
        """
        msg=None
        if not self._message_cache_queue.empty():
            msg = self._message_cache_queue.get()
        else:
            for new_msg in self.consume():
                self._message_cache_queue.put(new_msg)
            if not self._message_cache_queue.empty():
                msg = self._message_cache_queue.get()
        return msg

    def connect_by_cec_url(self, url: CecUrl):
        """Connect to Kafka server by CecUrl

        Args:
          url(str): CecUrl
        """
        self._kafka_consumer_client = ConfluentKafkaConsumer({
            'bootstrap.servers': url.netloc,
            "request.timeout.ms": 600000,
            **url.params
        })
        self._current_url = url.__str__()
        return self
    
    def connect(self, url:str):
        """connetct to server
        First use CecUrl.parse to generate an CecUrl Object from the 
        given url.
        Args:
            url (str): This url is a string.
            Contains protocal, netlocation, params, etc.

        Returns:
            _type_: KafkaConsumer

        
        """
        cec_url = CecUrl.parse(url)
        return self.connect_by_cec_url(cec_url)
    
    def disconnect(self):
        """Disconnect from Kafka server
        """
        if self._kafka_consumer_client is None:
            return
        
        self._kafka_consumer_client.close()
        self._kafka_consumer_client = None

    def __next__(self):
        msg = None
        try:
            if not self._message_cache_queue.empty():
                msg = self._message_cache_queue.get()
            else:
                # consume msg from remote server
                for new_msg in self.consume():
                    self._message_cache_queue.put(new_msg)
                if not self._message_cache_queue.empty():
                    msg = self._message_cache_queue.get()
        except Exception as e:
            pass
        finally:
            if msg is None:
                raise StopIteration()
        return msg
        