# -*- coding: utf-8 -*- #
"""
Time                2022/7/29 13:33
Author:             Zhangque (Wardenjohn)
Email               ydzhang@linux.alibaba.com
File                common.py
Description:        Static variable
"""

from cec_base.url import CecUrl

class StaticConst:
    """Static consts

    This class defines all the static constant values in the cec-redis module
    """

    # List of specialization parameters
    KAFKA_SPECIAL_PARM_CEC_BATCH_CONSUMER_LIMIT = "batch_consume_limit"
    KAFKA_SPECIAL_PARM_CEC_CONSUME_TIMEOUT = "kafka_consume_event_timeout"
    # KAFKA_CONNECTTION_PARAMS_ ****

    """
    The special parameter supported by Kafka
    """
    _kafka_special_parameter_list = [
        # batch_consume_limit => the number of event to consume in one batch
        #   1. Effective range：[Consumer]
        #   2. Meaning: This parameter specifies the number of event of one 
        #                consumer consume at one time. For some case of Kafka,
        #                if the number of event not reach the batch_consume_limit,
        #                the consumer process may block.
         KAFKA_SPECIAL_PARM_CEC_BATCH_CONSUMER_LIMIT,
        # kafka_consume_event_timeout => The time to wait for an event
        #   1. Effective range：[Consumer]
        #   2. Meaning: This parameter specifies the time to wait for an event.
        #               If the consumer wait for an event too long to reach the 
        #               limit of this timeout, consumer will stop to wait more
        #               event even though the number of message still not reach
        #               the batch_consume_limie.
         KAFKA_SPECIAL_PARM_CEC_CONSUME_TIMEOUT
    ]

    _kafka_special_parameters_default_value = {
        KAFKA_SPECIAL_PARM_CEC_BATCH_CONSUMER_LIMIT: (int, 1),
        KAFKA_SPECIAL_PARM_CEC_CONSUME_TIMEOUT: (int, 1)
    }

    """
    The following _kafka_connection_parameter_list is used to generate the supported
    parameter supported by the connection process to kafka server.
    _kafka_connection_parameters_default_value is used to generate the default value
    of kafka server connection.
    """

    _kafka_connection_parameter_list = [

    ]

    _kafka_connection_parameters_default_value = {

    }

    @staticmethod
    def parse_special_parameter(params: dict) -> dict:
        """Parse specialization parameters

        Parse the specialization parameters and remove the specialization
        parameters from the parameter list

        Args:
            params(dict): CecUrl.params

        Returns:

        """
        res = {}
        for key in StaticConst._kafka_special_parameter_list:
            _type, default = \
                StaticConst._kafka_special_parameters_default_value[key]
            res[key] = _type(params.pop(key, default))
        return res

    @staticmethod
    def parse_kafka_connection_params(params: dict) -> dict:
        """Parse kafka connection parameters

        Args:
            params(dict): CecUrl.params

        Returns:
            params(dict)
        """
        res = {}
        for key in StaticConst._kafka_connection_parameter_list:
            res[key] = params[key]
        return res


class ClientBase:
    """
    cec-kafka client base class, which provides some generic implementation
    """

    def __init__(self, url: CecUrl):
        self._redis_version = None
        self._special_params = StaticConst.parse_special_parameter(url.params)

    def get_special_param(self, key: str, default=''):
        """Get specialization parameter by key

        Args:
            key(str): specialization parameter key
            default(Any): default value if key not exists

        Returns:

        """
        return self._special_params.get(key, default)
