import setuptools

setuptools.setup(
    name="redis_lua",
    version="0.0.1",
    author="mingfeng(SunnyQjm)",
    author_email="mfeng@linux.alibaba.com",
    description="redis_lua is a library of tools that encapsulates some common lua scripts",
    url="",
    include_package_data=True,
    packages=setuptools.find_packages(),
    install_requires=[
        "redis"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
