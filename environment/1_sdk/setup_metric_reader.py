# -*- coding: utf-8 -*- #
"""
Time                2023/03/09 13:58
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                setup_sysom_utils.py
Description:
"""
import setuptools

setuptools.setup(
    name="metric_reader",
    version="0.0.1",
    author="mingfeng(SunnyQjm)",
    author_email="mfeng@linux.alibaba.com",
    description="A common metric reader interface",
    url="",
    packages=["metric_reader"],
    install_requires=[
        "requests>=2.27.1",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
