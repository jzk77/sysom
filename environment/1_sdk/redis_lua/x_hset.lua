-- 获取 KEY, FIELD, VALUE, EXPIRE
local expire = tonumber(ARGV[1])
if #ARGV < 3 then
    return "ERR: Missing filed,value"
end

-- 获取当前时间（以秒为单位）
local time = redis.call('TIME')
local expire_time = tonumber(time[1]) + tonumber(time[2]) / 1000000 + expire

for i = 2, #ARGV, 2 do
    local field = ARGV[i]
    local value = ARGV[i + 1]
    -- 保存键值
    redis.call("HSET", KEYS[1], field, value)
    if expire ~= -1 then
        -- 保存过期时间
        redis.call("HSET", KEYS[2], field, expire_time)
    end
end
return "OK"