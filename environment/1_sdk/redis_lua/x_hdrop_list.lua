local cursor = 0
repeat
    local result = redis.call('SCAN', cursor, 'MATCH', KEYS[1].."*", 'COUNT', 1000)
    cursor = tonumber(result[1])
    local keys = result[2]
    for i = 1, #keys do
        redis.call('DEL', keys[i])
    end
until cursor == 0