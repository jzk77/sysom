-- 获取哈希的所有子键和值
local hash_fields = redis.call('HGETALL', KEYS[1])

-- 获取当前时间（以秒为单位）
local time = redis.call('TIME')
local current_time = tonumber(time[1]) + tonumber(time[2]) / 1000000

local res = {}
-- 遍历所有子键和值
for i = 1, #hash_fields, 2 do
    -- 获取子键和值
    local field = hash_fields[i]
    local value = hash_fields[i + 1]
    local expire_time_str = redis.call("HGET", KEYS[2], field)
    local expire_time = -1
    if expire_time_str ~= false then
        expire_time = tonumber(expire_time_str)
    end
    if (expire_time >= 0 and expire_time < current_time) then
        -- 删除过期的成员
        redis.call("HDEL", KEYS[1], field)
        redis.call("HDEL", KEYS[2], field)
    else
        res[i] = field
        res[i + 1] = value
    end
end
return res