import setuptools

setuptools.setup(
    name="gclient_cmg",
    version="0.0.1",
    author="mingfeng(SunnyQjm)",
    author_email="mfeng@linux.alibaba.com",
    description="A cmg-based http client lib",
    url="",
    packages=setuptools.find_packages(),
    install_requires=[
        "cmg_base>=0.0.1"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ]
)
