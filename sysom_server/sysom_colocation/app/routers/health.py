# -*- coding: utf-8 -*- #
"""
Time                2023/12/20 19:47
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                health.py
Description:
"""
from fastapi import APIRouter


router = APIRouter()


@router.get("/check")
async def get_channel_config():
    return {
        "code": 0,
        "err_msg": "",
        "data": ""
    }
