# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from pydantic import BaseModel

###########################################################################
# Define schemas here
###########################################################################

class CecMessage(BaseModel):
    """
    CEC message
    """
    topic: str
    data: dict
# @reference https://fastapi.tiangolo.com/zh/tutorial/response-model/
# class Person(BaseModel):
#     id: int
#     name: str
#     age: int
    
#     class Config:
#         orm_mode = True