# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                crud.py
Description:
"""
from typing import Optional
from sqlalchemy.orm import Session
from app import models, schemas

################################################################################################
# Define database crud here
################################################################################################

# def get_person_by_name(db: Session, name: str) -> Optional[models.Person]:
#     return db.query(models.Person).filter(models.Person.name == name).first()

# def create_person(db: Session, person: schemas.Person) -> models.Person:
#     person = models.Person(**person.dict())
#     db.add(person)
#     db.commit()
#     db.refresh(person)
#     return person

# def del_person_by_id(db: Session, person_id: int):
#     person = db.get(models.Person, person_id)
#     db.delete(person)
#     db.commit()
