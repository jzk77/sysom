from abc import abstractmethod
from datetime import datetime
import json
import traceback
from typing import List
from uuid import uuid4
from clogger import logger

import numpy as np
from metric_reader.metric_reader import MetricReader, dispatch_metric_reader
from metric_reader.result import MetricResult
from metric_reader.task import InstantQueryTask, RangeQueryTask

from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask


STATUS_NORMAL = "NORMAL"
STATUS_WARNING = "WARNING"
STATUS_ERROR = "ERROR"
DEFAULT_EMPTY_STR = "N/A"

REASON_LLC = "LLC Miss 频发"
REASON_MBW = "内存带宽延迟过高"

SOURCE_LS = "在线业务"
SOURCE_BE = "离线业务"


class QueryError(Exception):
    pass


class TimeValueError(Exception):
    pass


class Diagnose:
    def __init__(self, check_name: str, reader: MetricReader) -> None:
        self._check_name = check_name
        self._status = STATUS_NORMAL
        self._result = "未检测到异常"
        self._suggestion = DEFAULT_EMPTY_STR
        self._reader = reader

    def get_overview(self) -> dict:
        return {
            "data": f"- **结论**: {self._result}\n- **修复建议**: {self._suggestion}\n\n"
        }

    @abstractmethod
    def diagnose(self) -> None:
        pass


class ContainerCpiRecord:
    def __init__(self, namespace: str, pod: str, container: str) -> None:
        # analyze result
        self._namespace = namespace
        self._pod = pod
        self._container = container
        self._err_count = 0
        self._err_core_reason = DEFAULT_EMPTY_STR
        self._err_core_source = DEFAULT_EMPTY_STR
        self._level = DEFAULT_EMPTY_STR
        # base data
        self._cpi = []
        self._llc_miss_rate = []
        self._err_bitmap = []
        self._imc_latency = []
        self._llc_dist = []
        self._mbw_dist = []
        # component data
        self._nr_llc = 0
        self._nr_mbw = 0
        self._nr_ls = 0
        self._nr_be = 0

    def record(
        self,
        cpi: float,
        llc_miss_rate: float,
        err: bool,
        latency: float,
        llc_dist: float,
        mbw_dist: float,
    ) -> None:
        self._cpi.append(cpi)
        self._llc_miss_rate.append(llc_miss_rate)
        self._err_bitmap.append(err)
        self._imc_latency.append(latency)
        self._llc_dist.append(llc_dist)
        self._mbw_dist.append(mbw_dist)
        if err:
            self._err_count += 1

    def analyze_level(self) -> None:
        # 分析告警等级
        if self._err_count < 3:
            self._level = "NORMAL"
        elif self._err_count < 5:
            self._level = "WARNING"
        else:
            self._level = "ERROR"

    def analyze_reason(self) -> None:
        err_avg_rate = 0
        normal_avg_rate = 0
        err_avg_latency = 0
        normal_avg_latency = 0
        nr_err = self._err_count
        nr_normal = len(self._err_bitmap) - self._err_count
        if nr_err == 0 or nr_normal == 0:
            return

        for err, miss_rate, latency in zip(
            self._err_bitmap, self._llc_miss_rate, self._imc_latency
        ):
            if err:
                err_avg_rate += miss_rate
                err_avg_latency += latency
            else:
                normal_avg_rate += miss_rate
                normal_avg_latency += latency
        err_avg_rate = err_avg_rate / nr_err
        err_avg_latency = err_avg_latency / nr_err
        normal_avg_rate = normal_avg_rate / nr_normal
        normal_avg_latency = normal_avg_latency / nr_normal

        # TODO 依赖于当前五分钟的数据 如果全是异常 则无法分析 待改成过去一天的平均延迟
        if normal_avg_rate == 0 or normal_avg_latency == 0:
            return

        # 分析每一次CPI异常的具体原因
        for err, miss_rate, latency, l3_occ_rate, mbw_rate in zip(
            self._err_bitmap,
            self._llc_miss_rate,
            self._imc_latency,
            self._llc_dist,
            self._mbw_dist,
        ):
            if not err:
                continue

            # 根据missrate和内存延迟的增幅判断主要原因
            if (miss_rate - normal_avg_rate) / normal_avg_rate > (
                latency - normal_avg_latency
            ) / normal_avg_latency:

                self._nr_llc += 1
                # 分析miss rate为主要原因时的干扰源
                if l3_occ_rate != float("inf") and l3_occ_rate > 0.5:
                    self._nr_ls += 1
                else:
                    self._nr_be += 1
            else:
                self._nr_mbw += 1
                if mbw_rate != float("inf") and mbw_rate > 0.5:
                    self._nr_ls += 1
                else:
                    self._nr_be += 1

        # 根据统计结果确定主要异常原因
        if self._nr_llc > self._nr_mbw:
            self._err_core_reason = REASON_LLC
        else:
            self._err_core_reason = REASON_MBW

        # 根据统计结果确定主要干扰源
        if self._nr_ls > self._nr_be:
            self._err_core_source = SOURCE_LS
        else:
            self._err_core_source = SOURCE_BE

    def analyze(self) -> None:
        self.analyze_level()
        self.analyze_reason()

    def report(self) -> dict:
        return {
            "key": str(uuid4()),
            "容器": f"{self._namespace}-{self._pod}-{self._container}",
            "干扰次数": self._err_count,
            "干扰等级": self._level,
            "主要干扰原因": self._err_core_reason,
            "干扰源": self._err_core_source,
        }

    def reason(self) -> str:
        return self._err_core_reason

    def err_count(self) -> int:
        return self._err_count

    def level(self) -> str:
        return self._level

    def source(self) -> str:
        return self._err_core_source


class ContainerTable:
    def __init__(self) -> None:
        self._container_table = {}
        self._nr_err = 0
        self._nr_err_container = 0
        self._nr_llc_miss = 0
        self._nr_mbw = 0

        self._status = DEFAULT_EMPTY_STR
        self._suggestion = DEFAULT_EMPTY_STR
        self._main_reason = DEFAULT_EMPTY_STR
        self._main_source = DEFAULT_EMPTY_STR

    def record(
        self,
        namespace: str,
        pod: str,
        container: str,
        cpi: float,
        llc_miss_rate: float,
        err: bool,
        latency: float,
        llc_dist: float,
        mbw_dist: float,
    ) -> None:
        key = f"{namespace}-{pod}-{container}"
        if key not in self._container_table.keys():
            self._container_table[key] = ContainerCpiRecord(namespace, pod, container)
        self._container_table[key].record(
            cpi, llc_miss_rate, err, latency, llc_dist, mbw_dist
        )

    def report(self) -> List[dict]:
        ret = []
        for record in self._container_table.values():
            if record.err_count() <= 3:
                continue
            ret.append(record.report())
        return ret

    def analyze(self) -> None:
        for record in self._container_table.values():
            if record.err_count() <= 3:
                continue
            self._nr_err_container += 1
            self._nr_err += record.err_count()
            record.analyze()
        self.analyze_main_reason()
        self.analyze_status()
        self.analyze_source()

    def analyze_main_reason(self) -> None:
        stat = {}
        for record in self._container_table.values():
            if record.err_count() <= 3:
                continue

            if record.reason() not in stat.keys():
                stat[record.reason()] = 0
            stat[record.reason()] += 1

        self._main_reason = DEFAULT_EMPTY_STR
        main_count = 0
        for reason, count in stat.items():
            if count > main_count:
                self._main_reason = reason
                main_count = count

    def analyze_status(self) -> None:
        # return the max level as the summary
        stat = {}
        for record in self._container_table.values():
            if record.err_count() <= 3:
                continue
            if record.level() not in stat.keys():
                stat[record.level()] = 0
            stat[record.level()] += 1

        if stat.get(STATUS_ERROR, None) is not None:
            self._status = STATUS_ERROR
        elif stat.get(STATUS_WARNING, None) is not None:
            self._status = STATUS_WARNING
        elif stat.get(STATUS_NORMAL, None) is not None:
            self._status = STATUS_NORMAL
        else:
            self._status = DEFAULT_EMPTY_STR

    def analyze_source(self) -> None:
        stat = {}
        for record in self._container_table.values():
            if record.err_count() <= 3:
                continue
            if record.source() not in stat.keys():
                stat[record.source()] = 0
            stat[record.source()] += 1
        self._main_source = DEFAULT_EMPTY_STR
        main_count = 0
        for reason, count in stat.items():
            if count > main_count:
                self._main_source = reason
                main_count = count

    def result(self) -> str:
        if self._status == STATUS_NORMAL or self._status == DEFAULT_EMPTY_STR:
            return f"未检测到异常"
        else:
            return f"诊断时刻存在CPI干扰，主要原因为{self._main_reason}，干扰的主要源头为{self._main_source}"

    def status(self) -> str:
        return self._status

    def suggestion(self) -> str:

        if self._status == STATUS_NORMAL or self._status == DEFAULT_EMPTY_STR:
            return DEFAULT_EMPTY_STR
        else:
            return (
                f"建议通过resctrl文件系统调整LLC和内存带宽分配策略或者改变Pod部署策略"
            )


class CpiSeries:
    def __init__(self) -> None:
        self._series = {}
        self.llc_occ = []
        self.mbw = []

    def record(self, timestamp: float, err_cnt: int) -> None:
        if timestamp not in self._series.keys():
            self._series[timestamp] = 0
        self._series[timestamp] += err_cnt

    def attach(self, llc_occ: List[float], mbw: List[float]) -> None:
        self.llc_occ = llc_occ
        self.mbw = mbw

    def report(self) -> List[dict]:
        ts_series = [ts for ts, _ in sorted(self._series.items())]
        err_series = [err for _, err in sorted(self._series.items())]
        combined = zip(ts_series, err_series, self.llc_occ, self.mbw)
        return [
            {
                "time": datetime.fromtimestamp(ts).strftime("%Y-%m-%d %H:%M:%S"),
                "CPI异常次数": err,
                "在线业务-LLC占比": llc,
                "在线业务-内存带宽占比": mbw,
            }
            for ts, err, llc, mbw in combined
        ]


class CpiDiagnose(Diagnose):

    def __init__(self, reader: MetricReader) -> None:
        super().__init__("CPI干扰", reader)
        self._series = CpiSeries()
        self._table = ContainerTable()

    def _calculate_rate(self, ls_data: dict, be_data: dict) -> List[float]:
        ls_matrix = [arr for arr in ls_data.values()]
        be_matrix = [arr for arr in be_data.values()]

        np_ls_vectors = np.array(ls_matrix)
        np_be_vectors = np.array(be_matrix)
        # 按列求和
        np_ls_sum_vector = np_ls_vectors.sum(axis=0)
        np_be_sum_vector = np_be_vectors.sum(axis=0)

        # 计算总和
        np_sum_vector = np_ls_sum_vector + np_be_sum_vector
        epsilon = 0.001
        safe_sum_vector = np.where(
            np.abs(np_sum_vector) < epsilon, epsilon, np_sum_vector
        )
        data = np_ls_sum_vector / safe_sum_vector
        # process the divide zero
        return data.tolist()

    def _query_5min_imc_data(self, instance: str, dt: datetime) -> List[float]:
        ts = dt.timestamp()
        task = (
            RangeQueryTask(
                "sysom_imc_event_node", start_time=int(ts - 300), end_time=int(ts)
            )
            .append_equal_filter("exported_instance", instance)
            .append_equal_filter("value", "rlat")
        )

        result = self._reader.range_query([task])
        if result.code != 0:
            raise QueryError(
                f"Query sysom_imc_event_node Failed. args: instance={instance} result: code={result.code} msg=({result.err_msg})"
            )

        if len(result.data) == 0:
            raise QueryError(
                f"Query sysom_imc_event_node empty. args: instance={instance} start={ts-300} end={ts}"
            )
        latency = [float(val[1]) for val in result.data[0].values]
        return latency

    def _query_5min_rdt_data(self, instance: str, dt: datetime) -> dict:
        """Query 5min rdt metric data.

        Args:
            instance (str): instance ip
            dt (datetime): query the 5min data before `dt`

        Raises:
            QueryError: if query failed or no data.

        Returns:
            dict: {'llc_occ': [float, float, ...], 'mbw': [float, float, ...]}
        """
        ts = dt.timestamp()
        task = (
            RangeQueryTask(
                "sysom_rdt_usage", start_time=int(ts - 300), end_time=int(ts)
            )
            .append_equal_filter("exported_instance", instance)
            .append_wildcard_filter("value", "llc_occ|total_mem_bw")
        )

        result = self._reader.range_query([task])
        if result.code != 0:
            raise QueryError(
                f"Query sysom_rdt_usage Failed. args: instance={instance} result: code={result.code} msg=({result.err_msg})"
            )
        data = {
            "LS": {"llc_occ": {}, "total_mem_bw": {}},
            "BE": {"llc_occ": {}, "total_mem_bw": {}},
        }
        """ the path follow format: sys/fs/resctrl/{tag}/mon_data/mon_L3_{socket}
            tag: BE or LS
            socket: 00, 01 ...
        """

        for item in result.data:
            labels = item.to_dict()["labels"]
            path = str(labels["path"])
            entries = path.split("/")
            if len(entries) != 6 or entries[4] != "mon_data":
                continue
            tag = entries[3]
            if tag != "LS" and tag != "BE":
                continue

            socket_tag = entries[5]
            data[tag][labels["value"]][socket_tag] = [
                float(val[1]) for val in item.to_dict()["values"]
            ]

        llc_occ_rate = self._calculate_rate(
            data["LS"]["llc_occ"], data["BE"]["llc_occ"]
        )
        mbw_rate = self._calculate_rate(
            data["LS"]["total_mem_bw"], data["BE"]["total_mem_bw"]
        )
        data = {"llc_occ_rate": llc_occ_rate, "mbw_rate": mbw_rate}
        return {"llc_occ_rate": llc_occ_rate, "mbw_rate": mbw_rate}

    def _querty_ls_miss_rate(self, instance: str, dt: datetime) -> List[float]:
        # get the miss rate between ls and be
        ts = dt.timestamp()
        task = (
            RangeQueryTask(
                "sysom_container_pmu_events", start_time=ts - 300, end_time=ts
            )
            .append_equal_filter("exported_instance", instance)
            .append_wildcard_filter("value", "llcStoreMis|llcLoadMis")
        )

        result = self._reader.range_query([task])

        if result.code != 0:
            raise QueryError(
                f"Query sysom_container_pmu_events Failed. args: instance={instance} result: code={result.code} msg=({result.err_msg})"
            )
        if len(result.data) == 0:
            raise QueryError(
                f"Query sysom_container_pmu_events data is empty. args: instance={instance})"
            )
        ls_vectors = []
        be_vectors = []

        for item in result.data:
            labels = item.to_dict()["labels"]
            tag = labels["bvt"]
            nr_miss = [float(val[1]) for val in item.values]
            if tag == "LS":
                ls_vectors.append(nr_miss)
            else:
                be_vectors.append(nr_miss)

        np_ls_vectors = np.array(ls_vectors)
        np_be_vectors = np.array(be_vectors)
        np_ls_sum_vector = np_ls_vectors.sum(axis=0)
        np_be_sum_vector = np_be_vectors.sum(axis=0)
        np_sum_vector = np_ls_sum_vector + np_be_sum_vector
        # process the divide zero
        data = np.where(
            np_sum_vector != 0, np_ls_vectors / np_sum_vector, np.nan
        ).tolist()
        return data

    def _query_5min_pmu_data(self, instance: str, dt: datetime) -> dict:
        """_summary_

        Args:
            instance (str): _description_
            dt (datetime): _description_

        Raises:
            QueryError: _description_

        Returns:
            dict: {'ns': str, 'pod': str, 'con': str, 'cpi: [...], 'lsMisRate' : [...]}
        """
        ts = dt.timestamp()
        container_list = {}

        task = (
            RangeQueryTask(
                "sysom_container_pmu_events", start_time=ts - 300, end_time=ts
            )
            .append_equal_filter("exported_instance", instance)
            .append_equal_filter("bvt", "LS")
            .append_wildcard_filter("value", "CPI|l3MisRate")
        )

        result = self._reader.range_query([task])

        if result.code != 0:
            raise QueryError(
                f"Query sysom_container_pmu_events Failed. args: instance={instance} result: code={result.code} msg=({result.err_msg})"
            )
        for item in result.data:
            labels = item.to_dict()["labels"]
            key = f"{labels['namespace']}:{labels['pod']}:{labels['container']}"

            if labels["container"] != "None" and key not in container_list.keys():
                container_list[key] = {
                    "ns": labels["namespace"],
                    "pod": labels["pod"],
                    "con": labels["container"],
                }
            container_list[key][labels["value"]] = item.values
        return container_list

    def _parse_aggregate_result(self, result: MetricResult) -> float:
        if result.code != 0:
            raise QueryError(
                f"Query stddev or mean failed. err_code={result.code} msg={result.err_msg}"
            )
        if len(result.data) == 0:
            raise QueryError(
                f"Query stddev or mean no data. Please check metric table."
            )

        return float(result.data[0].to_dict()["value"][1])

    def _query_yesterday_mean(
        self, instance: str, dt: datetime, namespace: str, pod: str, container: str
    ) -> float:
        return self._query_yesterday_agg(
            instance, dt, namespace, pod, container, "avg_over_time"
        )

    def _query_yesterday_stddev(
        self, instance: str, dt: datetime, namespace: str, pod: str, container: str
    ) -> float:
        return self._query_yesterday_agg(
            instance, dt, namespace, pod, container, "stddev_over_time"
        )

    def _query_yesterday_agg(
        self,
        instance: str,
        dt: datetime,
        namespace: str,
        pod: str,
        container: str,
        agg: str,
    ) -> float:
        zero_ts = dt.replace(hour=0, minute=0, second=0, microsecond=0).timestamp()
        task = (
            InstantQueryTask("sysom_container_pmu_events", zero_ts, agg, "1d")
            .append_equal_filter("exported_instance", instance)
            .append_equal_filter("namespace", namespace)
            .append_equal_filter("pod", pod)
            .append_equal_filter("container", container)
            .append_equal_filter("value", "CPI")
        )
        result = self._reader.instant_query([task])
        return self._parse_aggregate_result(result)

    def diagnose(self, instance: str, dt: datetime) -> None:
        container_list = self._query_5min_pmu_data(instance, dt)
        imc_lat_list = self._query_5min_imc_data(instance, dt)
        rdt_data = self._query_5min_rdt_data(instance, dt)
        for item in container_list.values():
            cpi_avg = self._query_yesterday_mean(
                instance, dt, item["ns"], item["pod"], item["con"]
            )
            cpi_stddev = self._query_yesterday_stddev(
                instance, dt, item["ns"], item["pod"], item["con"]
            )

            for cpi, l3_miss_rate, imc_lat, occ_rate, mbw_rate in zip(
                item["CPI"],
                item["l3MisRate"],
                imc_lat_list,
                rdt_data["llc_occ_rate"],
                rdt_data["mbw_rate"],
            ):
                ts = float(cpi[0])
                cpi_val = float(cpi[1])
                miss_val = float(l3_miss_rate[1])
                err = cpi_val != 0 and cpi_val >= cpi_avg + 2 * cpi_stddev
                if err:
                    # 2. 时间序列累计每个时刻的异常次数
                    self._series.record(timestamp=ts, err_cnt=1)
                else:
                    self._series.record(timestamp=ts, err_cnt=0)

                # 3. 容器维度累计容器的异常次数 同时分析异常时刻的异常原因是llc miss rate还是内存延迟
                self._table.record(
                    namespace=item["ns"],
                    pod=item["pod"],
                    container=item["con"],
                    cpi=cpi_val,
                    llc_miss_rate=miss_val,
                    err=err,
                    latency=imc_lat,
                    llc_dist=occ_rate,
                    mbw_dist=mbw_rate,
                )
        self._series.attach(rdt_data["llc_occ_rate"], rdt_data["mbw_rate"])
        self._table.analyze()
        self._status = self._table.status()
        self._suggestion = self._table.suggestion()
        self._result = self._table.result()

    def get_timeseries(self) -> dict:
        return {"data": self._series.report()}

    def get_container_table(self) -> dict:
        return {"data": self._table.report()}


def diagnose(instance: str, dt: datetime) -> dict:
    result = {}
    metric_reader = dispatch_metric_reader("prometheus://localhost:9090")
    cpi_diag = CpiDiagnose(metric_reader)
    cpi_diag.diagnose(instance, dt)

    result["overview"] = cpi_diag.get_overview()
    result["container-table"] = cpi_diag.get_container_table()
    result["disturb-timeseries"] = cpi_diag.get_timeseries()
    return result


def validate_time(time_str: str) -> datetime:
    try:
        return datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
    except ValueError:
        raise TimeValueError(f"Time format error. time_str={time_str}")


class PreProcessor(DiagnosisPreProcessor):
    """Command diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        result = {"code": 0, "err_msg": "", "result": {}}
        try:
            instance = params.get("instance", "")
            # process host:port, we only use host
            instance = str(instance).split(":")[0]
            time_str = params.get("moment", "")
            dt = datetime.now() if time_str == "" else validate_time(time_str)
            result["result"] = diagnose(instance, dt)
        except Exception as e:
            logger.error(f"Diagnose error. err={e}")
            traceback.print_exc()
            result = {
                "code": 1,
                "err_msg": f"{str(e)}\n解决方法：请检查是否是混部场景、是否支持硬件指标采集、是否开启SysAK对应插件",
                "result": {},
            }
        finally:
            return DiagnosisTask(
                jobs=[DiagnosisJob(instance="", cmd="")],
                offline_mode=True,
                offline_results=[json.dumps(result)],
            )
