from service_scripts.base import DiagnosisTask


class DiagnosisPreProcessorPostWrapperBase:
    """诊断前处理后包装器，用于在诊断前处理后对前处理的结果进行统一加工，比如给命令加上 wrapper 等
    """
    def process(self, task_id: str, diagnosis_task: DiagnosisTask):
        pass
