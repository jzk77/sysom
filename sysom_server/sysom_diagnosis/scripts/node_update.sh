#!/bin/bash -x
RESOURCE_DIR=${NODE_HOME}/${SERVICE_NAME}

if [ "$SYSAK_VERTION" == "" ]; then
    export SYSAK_VERTION=3.2.0-1
fi
if [ "$ARCH" == "" ]; then
    export ARCH=x86_64
fi

main()
{
    ###we need to update the sysak rpm###
    rpm -qa | grep sysak
    if [ $? -eq 0 ]
    then
        yum erase -y `rpm -qa | grep sysak`
    fi

    SYSAK_PKG=sysak-${SYSAK_VERTION}.${ARCH}.rpm
    yum install -y ${SYSAK_PKG}
    exit $?
}

main
