# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                ssh.py
Description:
"""
from clogger import logger
from fastapi import FastAPI
from app.routers import log, health
from conf.settings import YAML_CONFIG
from sysom_utils import CmgPlugin, SysomFramework


app = FastAPI()


app.include_router(health.router, prefix="/api/v1/log/health")
app.include_router(log.router, prefix="/api/v1/log")


#############################################################################
# Write your API interface here, or add to app/routes
#############################################################################


def init_framwork():
    SysomFramework\
        .init(YAML_CONFIG) \
        .load_plugin_cls(CmgPlugin) \
        .start()
    logger.info("SysomFramework init finished!")


def init_log_service():
    from app.executor import LogEventListener
    try:
        LogEventListener().start()
    except Exception as e:
        logger.exception(e)


@app.on_event("startup")
async def on_start():
    init_framwork()
    init_log_service()

    #############################################################################
    # Perform some microservice initialization operations over here
    #############################################################################


@app.on_event("shutdown")
async def on_shutdown():
    pass
