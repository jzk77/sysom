# -*- coding: utf-8 -*-
"""
@Author: wb-msm241621
@Date: 2023-07-19 15:40:32
@LastEditTime: 2023-07-19 15:40:33
@Description: 
"""
from typing import Union
from fastapi import APIRouter, status, Depends
from sqlalchemy.orm import Session
from app.database import get_db
from app.schemas import NodeLog, AuditLog
from app.query import NodeLogQueryParams, AuditLogQueryParams
from app.crud import get_node_logs, get_audit_logs
from sysom_utils import StandardListResponse, StandardResponse


router = APIRouter()


@router.get("/node")
async def audit_log_list(
    query_params: NodeLogQueryParams = Depends(), db: Session = Depends(get_db)
):
    node_log_list, total_count = get_node_logs(db, query_params)
    return StandardListResponse(node_log_list, NodeLog, total=total_count)


@router.get("/audit")
async def node_log_list(
    query_params: AuditLogQueryParams = Depends(), db: Session = Depends(get_db)
):
    audit_log_list, total_count = get_audit_logs(db, query_params)
    return StandardListResponse(audit_log_list, AuditLog, total=total_count)
