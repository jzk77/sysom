# -*- coding: utf-8 -*-
'''
@Author: wb-msm241621
@Date: 2024-01-31 17:59:13
@LastEditTime: 2024-01-31 17:59:13
@Description: 
'''
from fastapi import APIRouter
from conf.settings import *
from cmg_base import dispatch_service_discovery
from sysom_utils import StandardResponse, StandardListResponse
from ..schemas import ServiceItemModel

router = APIRouter()


discovery = dispatch_service_discovery(YAML_CONFIG.get_cmg_url())


@router.get("/list")
async def list_services():
    try:
        services = discovery.get_services()
        servicesResponseList = [
            ServiceItemModel(
                service_name=service,
                count=discovery.get_instance_count(service)
            ) for service in services
        ]
    except Exception as exc:
        return StandardResponse.error(str(exc))
    return StandardListResponse.success(servicesResponseList)
