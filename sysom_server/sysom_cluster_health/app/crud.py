# -*- coding: utf-8 -*- #
"""
Time                2023/11/29 10:08
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                crud.py
Description:
"""
import uuid
from clogger import logger
from typing import Optional, List
from sqlalchemy.orm import Session
from app import models, schemas, query

################################################################################################
# Define database crud here
################################################################################################

# def get_person_by_name(db: Session, name: str) -> Optional[models.Person]:
#     return db.query(models.Person).filter(models.Person.name == name).first()

# def create_person(db: Session, person: schemas.Person) -> models.Person:
#     person = models.Person(**person.dict())
#     db.add(person)
#     db.commit()
#     db.refresh(person)
#     return person

# def del_person_by_id(db: Session, person_id: int):
#     person = db.get(models.Person, person_id)
#     db.delete(person)
#     db.commit()

# def get_person_list(db: Session, query_params: query.PersonQueryParams) -> List[models.Person]:
#     return (
#         query_params.get_query_exp(db)
#         .all()
#     )


def create_abnormal_metrics_data(
    db: Session, abnormal_metrics: schemas.AbnormalMetricsBase
) -> Optional[models.BaseModel]:
    base = {
        "uuid": str(uuid.uuid4()),
        "metric_id": abnormal_metrics.metric_id,
        "metric_type": abnormal_metrics.metric_type,
        "score": abnormal_metrics.score,
        "value": abnormal_metrics.value,
        "cluster": abnormal_metrics.cluster,
        "timestamp": abnormal_metrics.timestamp
    }

    if abnormal_metrics.instance == "":
        db_abnormal_metrics = models.AbnormalMetricsCluster(**base)
    elif abnormal_metrics.pod == "" and abnormal_metrics.instance != "":
        db_abnormal_metrics = models.AbnormalMetricsNode(
            instance=abnormal_metrics.instance, 
            **base
        )
    elif abnormal_metrics.pod != "" and abnormal_metrics.namespace != "":
        db_abnormal_metrics = models.AbnormalMetricsPod(
            instance=abnormal_metrics.instance,
            pod=abnormal_metrics.pod,
            namespace=abnormal_metrics.namespace,
            **base,
        )
    else:
        logger.error(f"Inserting Invalid abnormal_metrics "
                     f"to mysql: {abnormal_metrics}")
        return None

    db.add(db_abnormal_metrics)
    db.commit()
    db.refresh(db_abnormal_metrics)
    return db_abnormal_metrics

def del_all_abnormal_metrics_data(db: Session):
    db.query(models.AbnormalMetricsCluster).delete()
    db.query(models.AbnormalMetricsNode).delete()
    db.query(models.AbnormalMetricsPod).delete()
    db.commit()
    return