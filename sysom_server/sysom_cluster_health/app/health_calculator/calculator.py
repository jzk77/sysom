import time
import copy
from typing import Dict, Optional
from conf.settings import *
from multiprocessing import Process
from schedule import Scheduler
from os import getpid, kill
from clogger import logger
from sysom_utils import SysomFramework, GCache
from lib.common_type import Level
from app.health_calculator.instance import construct_cluster_infos
from app.health_calculator.algorithm.health_algorithm import choose_algorithm, HealthAlgorithm
from app.crud import del_all_abnormal_metrics_data
from app.database import SessionLocal
from metric_reader import dispatch_metric_reader


class HealthCalculator(Process):
    gcache: Optional[Dict[Level, GCache]] = None
    algorithms: Optional[Dict[Level, HealthAlgorithm]] = None
    
    def __init__(
        self,
        parent_pid: int = None
    ) -> None:
        super().__init__(daemon=True)
        self.clusterhealth_interval = CALCULATE_INTERVAL
        self.clusterhealth_host_schedule: Scheduler = Scheduler()
        self.parent_pid = parent_pid
        self.cluster_infos = {}
        self.metric_reader = dispatch_metric_reader(
            "prometheus://" + PROMETHEUS_CONFIG.host + ":" + str(PROMETHEUS_CONFIG.port))
    
    @classmethod
    def get_gcache(cls) -> Dict[Level, GCache]:
        if cls.gcache is None:
            cls.gcache = {
                Level.Cluster: SysomFramework.gcache(CLUSTER_HEALTH_METRIC_GCACHE),
                Level.Node: SysomFramework.gcache(NODE_HEALTH_METRIC_GCACHE),
                Level.Pod: SysomFramework.gcache(POD_HEALTH_METRIC_GCACHE)
            }
        return cls.gcache
    
    @classmethod
    def get_algorithms(cls) -> Dict[Level, HealthAlgorithm]:
        if cls.algorithms is None:
            cls.algorithms = {
                Level.Cluster: choose_algorithm(CLUSTER_ALGORITHM, Level.Cluster),
                Level.Node: choose_algorithm(NODE_ALGORITHM, Level.Node),
                Level.Pod: choose_algorithm(POD_ALGORITHM, Level.Pod)
            }
        return cls.algorithms
        
    def _check_if_parent_is_alive(self):
        try:
            kill(self.parent_pid, 0)
        except OSError:
            logger.info(f"Analyzer's parent {self.parent_pid} is exit")
            exit(0)
            
    def _calculate_health(self):
        for cluster in self.cluster_infos.values():
            cluster.collect_metrics(
                HealthCalculator().get_gcache()[Level.Cluster]
            )
            for node in cluster.nodes.values():
                node.collect_metrics(
                    HealthCalculator().get_gcache()[Level.Node]
                )
                for pod in node.pods.values():
                    # collect metrics from gcache
                    pod.collect_metrics(
                        HealthCalculator().get_gcache()[Level.Pod]
                    )
                    # calculate pod health score
                    pod.calculate_health(copy.deepcopy(
                        HealthCalculator().get_algorithms()[Level.Pod]
                    ))
                # calculate node health score
                node.calculate_health(copy.deepcopy(
                    HealthCalculator().get_algorithms()[Level.Node]
                ))
            # calculate cluster health score
            cluster.calculate_health(copy.deepcopy(
                HealthCalculator().get_algorithms()[Level.Cluster]
            ))
                
    def calculating_task(self):
        start_time = time.time()
        
        # cleanup abnormal metrics of last round data from mysql
        if ABNORMAL_METRIC_STORAGE == "mysql":
            with SessionLocal() as db:
                del_all_abnormal_metrics_data(db)

        try:
            self.cluster_infos = construct_cluster_infos(self.metric_reader, 
                                                   self.clusterhealth_interval)
        except Exception as e:
            logger.error(f"Failed to construct cluster infos: {e}")
            return
        
        self._calculate_health()
        
        # cleanup metric set from gcache
        HealthCalculator().get_gcache()[Level.Cluster].clean()
        HealthCalculator().get_gcache()[Level.Node].clean()
        HealthCalculator().get_gcache()[Level.Pod].clean()
        
        self.last_end_time = time.time()
        end_time = time.time()
        logger.info(f"Excutaion time: {end_time - start_time}")


    def run(self) -> None:
        logger.info(f'健康度计算守护进程PID： {getpid()}')

        self.calculating_task()
        self.clusterhealth_host_schedule.every(self.clusterhealth_interval)\
            .seconds.do(self.calculating_task)

        while True:
            self._check_if_parent_is_alive();
            
            if self.is_alive():
                self.clusterhealth_host_schedule.run_pending()
            else:
                break
            time.sleep(max(1, int(self.clusterhealth_interval / 2)))