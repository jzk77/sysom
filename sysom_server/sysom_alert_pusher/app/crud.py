# -*- coding: utf-8 -*- #
"""
Time                2023/11/23 19:11
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                crud.py
Description:
"""
from typing import Optional, List
from sqlalchemy.orm import Session
from app import models, schemas, query

################################################################################################
# Define database crud here
################################################################################################

# def get_person_by_name(db: Session, name: str) -> Optional[models.Person]:
#     return db.query(models.Person).filter(models.Person.name == name).first()

# def create_person(db: Session, person: schemas.Person) -> models.Person:
#     person = models.Person(**person.dict())
#     db.add(person)
#     db.commit()
#     db.refresh(person)
#     return person

# def del_person_by_id(db: Session, person_id: int):
#     person = db.get(models.Person, person_id)
#     db.delete(person)
#     db.commit()

# def get_person_list(db: Session, query_params: query.PersonQueryParams) -> List[models.Person]:
#     return (
#         query_params.get_query_exp(db)
#         .all()
#     )
