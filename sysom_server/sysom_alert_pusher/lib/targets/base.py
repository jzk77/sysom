from abc import ABCMeta, abstractmethod
from app.schemas import AlertData

class PushTargetBase(metaclass=ABCMeta):
    
    def __init__(self, config: dict) -> None:
        self.config = config
    
    async def push_alert_data(self, alert_data: AlertData):
        """Push alert data to the target.
        """
        await self.push(alert_data.dict())
    
    @abstractmethod
    async def push(self, data: dict):
        pass