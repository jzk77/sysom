from fastapi import FastAPI
from conf.settings import *
from app.routeres import home, services, health, grafana, prometheus
from sysom_utils import CmgPlugin, SysomFramework, NodeDispatcherPlugin

app = FastAPI()

app.include_router(home.router, prefix="/api/v1/monitor/home")
app.include_router(grafana.router, prefix="/api/v1/monitor/grafana")
app.include_router(prometheus.router, prefix="/api/v1/monitor/prometheus")
app.include_router(services.router, prefix="/api/v1/monitor/services")
app.include_router(health.router, prefix="/api/v1/monitor/health")

@app.on_event("startup")
async def on_start():
    SysomFramework.load_plugin_cls(CmgPlugin) \
        .load_plugin_cls(NodeDispatcherPlugin) \
        .enable_channel_job() \
        .start()


@app.on_event("shutdown")
async def on_shutdown():
    pass
