# -*- coding: utf-8 -*- 
'''
@Author: wb-msm241621
@Date: 2023-12-15 11:40:19
@LastEditTime: 2023-12-15 11:40:19
@Description: hotfix released list filter
'''

from django_filters.rest_framework import FilterSet, DateTimeFilter
from .models import ReleasedHotfixListModule


class HotfixReleasedFilter(FilterSet):
    released_start_time = DateTimeFilter(field_name='released_time', lookup_expr='gte')
    released_end_time = DateTimeFilter(field_name='released_time', lookup_expr='lte')
    
    class Meta:
        model = ReleasedHotfixListModule
        fields = ['hotfix_id', 'released_kernel_version', 'serious', 'fix_system']
