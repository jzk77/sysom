#!/usr/bin/python3
"""
# Please follow the input of this script
# input : kernel_version : eg. 4.19.91-26.an7.x86_64
# output : branch generated from the logic given by user from the given kernel_version 
# User who just fulfill the branch generating logic in function "generate_branch" will be enough
"""
import os
import sys

def generate_branch(kernel_version):
    # finish your branch generating logic from kernel version here ...
    return kernel_version

if __name__ == "__main__":
    kernel_version = sys.argv[1]
    print(generate_branch(kernel_version))